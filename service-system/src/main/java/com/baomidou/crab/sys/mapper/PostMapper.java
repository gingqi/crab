package com.baomidou.crab.sys.mapper;

import com.baomidou.crab.sys.entity.Post;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统岗位表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2018-10-21
 */
public interface PostMapper extends BaseMapper<Post> {

}
